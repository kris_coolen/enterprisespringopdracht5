package be.kriscoolen.cursusenterprisespring.opdracht5;

import be.kriscoolen.cursusenterprisespring.opdracht5.domain.Beer;
import be.kriscoolen.cursusenterprisespring.opdracht5.repository.BeerRepository;
import be.kriscoolen.cursusenterprisespring.opdracht5.repository.BrewerRepository;
import be.kriscoolen.cursusenterprisespring.opdracht5.repository.CategoryRepository;
import be.kriscoolen.cursusenterprisespring.opdracht5.service.BeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.remoting.jaxws.SimpleJaxWsServiceExporter;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.core.AuthenticatedPrincipal;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.sql.DataSource;
import java.util.List;

@SpringBootApplication
//@EnableGlobalMethodSecurity(securedEnabled = true)
@EnableJpaRepositories
public class BeerApp {
    /*@Autowired
    public void configureSecurity(AuthenticationManagerBuilder auth, DataSource ds) throws Exception {
        auth.jdbcAuthentication()
                .passwordEncoder(new BCryptPasswordEncoder())
                .dataSource(ds)
                .usersByUsernameQuery("select name, passwordbc, enabled from Users where name=?")
                .authoritiesByUsernameQuery("select name, role from Users where name=?");
    }*/

    public static void main(String[] args) {

      /*  SecurityContext sc = SecurityContextHolder.getContext();
       // Authentication auth = new UsernamePasswordAuthenticationToken("homer","password");
        Authentication auth = new UsernamePasswordAuthenticationToken("bart","password");
        sc.setAuthentication(auth);*/


        //ConfigurableApplicationContext ctx = SpringApplication.run(BeerApp.class, args);



     /*  BeerRepository beerRepository = ctx.getBean("beerRepository",BeerRepository.class);

        System.out.println("Beer with id=20:");
        Beer beer = beerRepository.getBeerById(20);
        System.out.println(beer);

        System.out.println("Beers with alcohol 1.0:");
        List<Beer> beers = beerRepository.getBeersByAlcohol(1.0f);
        System.out.println(beers);

        Beer updateBeer = beerRepository.getBeerById(4);
        System.out.println("Beer with id 4:");
        System.out.println(updateBeer);
        System.out.println("We update the stock to 100 and price to 2.50");
        updateBeer.setStock(100);
        updateBeer.setPrice(2.50f);
        beerRepository.updateBeer(updateBeer);
        System.out.println("Check in DB if beer is updated:");
        System.out.println(beerRepository.getBeerById(4));

        System.out.println("Beers with 'blond' in their name:");
        System.out.println(beerRepository.findBeersByNameIgnoreCaseContainingOrderByNameAsc("blond"));

     System.out.println("All beers with a stock less than 100:");
     System.out.println(beerRepository.findBeersByStockLessThan(100));

    *//* System.out.println("price update of all beers with 5 %");
     float oldPrice = beerRepository.getBeerById(4).getPrice();
     System.out.println(oldPrice);
     System.out.println(beerRepository.updatePrice(1.05f));
     float newPrice = beerRepository.getBeerById(4).getPrice();
     System.out.println(newPrice);*//*
     System.out.println("beers with alc 9 and name starting with st:");
     System.out.println(beerRepository.findBeersWithAlcoholAndStartingWith(9,"st"));
     System.out.println("aantal bieren in de db = " + beerRepository.findAll().size());
     boolean beerExistsWithId15 = beerRepository.existsById(15);
     if(beerExistsWithId15){
      System.out.println("Beer with id 15 exists");
     }
     else System.out.println("Beer with id 15 does not exist");

     BrewerRepository brewerRepository = ctx.getBean("brewerRepository",BrewerRepository.class);
     System.out.println("brewer with id=2:");
     System.out.println(brewerRepository.findBrewerById(2));
     System.out.println("list of all brewers:");
     System.out.println(brewerRepository.findAll());
     CategoryRepository categoryRepository = ctx.getBean("categoryRepository",CategoryRepository.class);
     System.out.println("Beer category with id=42:");
     System.out.println(categoryRepository.findCategoryById(42));
     System.out.println("list of all categories:");
     System.out.println(categoryRepository.findAll());*/


        /*BeerService beerService = ctx.getBean("beerService", BeerService.class);
        try {
            beerService.orderBeer("bier-bestelling", 5, -5);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        try {
            beerService.orderBeer("bier-bestelling", 50000, 10);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        try {
            beerService.orderBeer("bier-bestelling", 5, 100000);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        try {
            beerService.orderBeer("bier-bestelling", 5, 25);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }*/

        SpringApplication.run(BeerApp.class, args);
    }
    @Bean
    public SimpleJaxWsServiceExporter exporter(){
        SimpleJaxWsServiceExporter exporter = new SimpleJaxWsServiceExporter();
        exporter.setBaseAddress("http://localhost/");
        return exporter;
    }
}
