package be.kriscoolen.cursusenterprisespring.opdracht5.repository;

import be.kriscoolen.cursusenterprisespring.opdracht5.domain.BeerOrder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BeerOrderRepository extends JpaRepository<BeerOrder,Integer> {
    public default int saveOrder(BeerOrder order){
        saveAndFlush(order);
        return order.getId();
    }
    public default BeerOrder getBeerOrderById(int id){
        return findById(id).orElse(null);
    }
}
