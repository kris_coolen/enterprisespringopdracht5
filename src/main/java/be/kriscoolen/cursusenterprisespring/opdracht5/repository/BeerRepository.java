package be.kriscoolen.cursusenterprisespring.opdracht5.repository;

import be.kriscoolen.cursusenterprisespring.opdracht5.domain.Beer;
import be.kriscoolen.cursusenterprisespring.opdracht5.domain.Brewer;
import be.kriscoolen.cursusenterprisespring.opdracht5.domain.Category;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface BeerRepository extends JpaRepository<Beer,Integer> {

    @Transactional(readOnly = true)
    public default Beer getBeerById(int id){
        return findById(id).orElse(null);
    }

    @Transactional(readOnly = true)
    @Query(value = "select b from Beer b where b.alcohol=:param_alcohol")
    public List<Beer> getBeersByAlcohol(@Param("param_alcohol") float alcohol);

    @Transactional(readOnly = true)
    public List<Beer> findBeersByNameIgnoreCaseContainingOrderByNameAsc(String partOfName);

    @Transactional(readOnly = true)
    public List<Beer> findBeersByStockLessThan(int max);

    @Transactional
    public default void updateBeer(Beer beer){
        save(beer);
    }

    @Transactional
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    //@Modifying
    @Query("update Beer b set b.price=b.price*?1")
    public int updatePrice(float percentage);

    @Transactional(readOnly = true)
    public default List<Beer> findBeersWithAlcoholAndStartingWith(float alcohol,String prefix){
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withIgnorePaths("version","stock","id","price")
                .withMatcher("name",m->m.ignoreCase().startsWith());
        Beer probe = new Beer();
        probe.setAlcohol(alcohol);
        probe.setName(prefix);
        Example<Beer> example = Example.of(probe,matcher);
        return findAll(example);
    }

    public List<Beer> findBeersByCategory(Category category);
    //select b Beer as b join b.category as c where c:=category

    public List<Beer> findBeersByBrewer(Brewer brewer);


}
