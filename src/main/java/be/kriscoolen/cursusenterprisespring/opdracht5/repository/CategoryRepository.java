package be.kriscoolen.cursusenterprisespring.opdracht5.repository;

import be.kriscoolen.cursusenterprisespring.opdracht5.domain.Beer;
import be.kriscoolen.cursusenterprisespring.opdracht5.domain.Brewer;
import be.kriscoolen.cursusenterprisespring.opdracht5.domain.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CategoryRepository extends JpaRepository<Category,Integer> {

    public default Category findCategoryById(Integer id){
        return findById(id).orElse(null);
    }

}
