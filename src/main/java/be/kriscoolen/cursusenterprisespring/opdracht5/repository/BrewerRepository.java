package be.kriscoolen.cursusenterprisespring.opdracht5.repository;

import be.kriscoolen.cursusenterprisespring.opdracht5.domain.Beer;
import be.kriscoolen.cursusenterprisespring.opdracht5.domain.Brewer;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.RepositoryDefinition;


import java.util.List;
import java.util.Optional;

@RepositoryDefinition(domainClass = Brewer.class,idClass = Integer.class)
//public interface BrewerRepository extends Repository<Brewer,Integer> {
public interface BrewerRepository{

    public void save(Brewer brewer); //from crudrepository
    public Optional<Brewer> findById(Integer id);//from crudrepository
    public default Brewer findBrewerById(Integer id){
       //your own findbyid to prevent optional return type
        return findById(id).orElse(null);
    }
    public List<Brewer> findAll();//from crudrepository


}
