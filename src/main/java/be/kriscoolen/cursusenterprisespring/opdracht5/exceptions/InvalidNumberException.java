package be.kriscoolen.cursusenterprisespring.opdracht5.exceptions;

public class InvalidNumberException extends RuntimeException {

    public InvalidNumberException(String message){
        super(message);
    }
}
