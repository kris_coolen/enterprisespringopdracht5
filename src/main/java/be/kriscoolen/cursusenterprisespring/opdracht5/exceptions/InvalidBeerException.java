package be.kriscoolen.cursusenterprisespring.opdracht5.exceptions;

public class InvalidBeerException extends RuntimeException {

    public InvalidBeerException(String message){
        super(message);
    }
}
