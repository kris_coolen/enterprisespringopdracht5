package be.kriscoolen.cursusenterprisespring.opdracht5.service;

import be.kriscoolen.cursusenterprisespring.opdracht5.domain.Beer;
import be.kriscoolen.cursusenterprisespring.opdracht5.exceptions.InvalidBeerException;
import be.kriscoolen.cursusenterprisespring.opdracht5.exceptions.InvalidNumberException;
import be.kriscoolen.cursusenterprisespring.opdracht5.repository.BeerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@Service
@WebService(serviceName = "BeerService")
@SOAPBinding
public class BeerServiceEndPoint {

    @Autowired
    private BeerService beerService;

    @Autowired
    private BeerRepository beerRepository;

    @WebMethod
    @WebResult
    public int orderBeer(@WebParam(name="orderName") String name,@WebParam(name="beerId") int id,
                         @WebParam(name="number") int number) throws InvalidBeerException, InvalidNumberException
    {
        return beerService.orderBeer(name,id,number);
    }

    @WebMethod
    @WebResult
    public int orderBeers(@WebParam(name="orderName") String name, @WebParam(name="order") int[][] order)
        throws InvalidBeerException, InvalidNumberException
    {
        return beerService.orderBeers(name,order);
    }

    @WebMethod
    @WebResult
    public Beer getBeerById(@WebParam(name="id") int id){
        return beerRepository.getBeerById(id);
    }

}
