package be.kriscoolen.cursusenterprisespring.opdracht5.service;

import be.kriscoolen.cursusenterprisespring.opdracht5.domain.Beer;
import be.kriscoolen.cursusenterprisespring.opdracht5.domain.BeerOrder;
import be.kriscoolen.cursusenterprisespring.opdracht5.domain.BeerOrderItem;
import be.kriscoolen.cursusenterprisespring.opdracht5.exceptions.InvalidBeerException;
import be.kriscoolen.cursusenterprisespring.opdracht5.exceptions.InvalidNumberException;
import be.kriscoolen.cursusenterprisespring.opdracht5.repository.BeerOrderRepository;
import be.kriscoolen.cursusenterprisespring.opdracht5.repository.BeerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service("beerService")
public class BeerServiceImpl implements BeerService {


    BeerRepository beerRepository;
    BeerOrderRepository beerOrderRepository;

    @Autowired
    public BeerServiceImpl(BeerRepository beerRepository, BeerOrderRepository beerOrderRepository){
        this.beerRepository = beerRepository;
        this.beerOrderRepository = beerOrderRepository;
    }

    public BeerServiceImpl(){

    }


    @Secured({"ROLE_ADULT"})
    @Override
    //@Transactional(rollbackFor = {InvalidNumberException.class,InvalidBeerException.class})
    @Transactional
    public int orderBeer(String newOrderName, int beerId, int number) throws InvalidBeerException, InvalidNumberException {

       /* if(number<0){throw new InvalidNumberException("number of beers to order cannot be negative!");}
        Beer beer = beerRepository.getBeerById(beerId);
        if(beer==null){throw new InvalidBeerException("could not find beer with beerId="+beerId);}
        if(beer.getStock()<number) {
            String message = "You want to order "+number + " beers with beerId=" + beerId +
                    ". You can order atmost " + beer.getStock() + " beers!";
            throw new InvalidNumberException(message);
        }
        //beerorder can be forfilled. We adjust the stock of the beer, and save the order!
        beer.setStock(beer.getStock()-number);
        beerRepository.updateBeer(beer);

        BeerOrder beerOrder= new BeerOrder(newOrderName);
        beerOrder.addBeerOrderItem(new BeerOrderItem(beer,number));
        return beerOrderRepository.saveOrder(beerOrder);*/
        int[][] order = new int[1][2];
        order[0][0] = beerId;
        order[0][1] = number;
        return orderBeers(newOrderName,order);
    }

    @Secured({"ROLE_ADULT"})
    @Override
    //@Transactional(rollbackFor = {InvalidNumberException.class,InvalidBeerException.class})
    @Transactional
    public int orderBeers(String orderName, int[][] order) throws InvalidBeerException, InvalidNumberException {
        List<Beer> beers = new ArrayList<>();
        for(int i=0; i<order.length;i++){
            if(order[i][1]<0){
                throw new InvalidNumberException("number of beers to order cannot be negative!");}
            Beer beer = beerRepository.getBeerById(order[i][0]);
            if(beer==null) {
                throw new InvalidBeerException("could not find beer with beerId="+order[i][0]);
            }
            if(beer.getStock()<order[i][1]){
                String message = "You want to order "+order[i][1] + " beers with beerId=" + order[i][0] +
                        ". You can order atmost " + beer.getStock() + " beers!";
                throw new InvalidNumberException(message);
            }
            //nu zijn we zeker dat het i-de bier dat je wil bestellen ok is. Dit steken we in de lijst met bieren
            beers.add(beer);
        }
        //Op dit punt zijn we zeker dat we onze order correct kunnen afhandelen
        BeerOrder beerOrder = new BeerOrder(orderName);
        for(int i=0; i<order.length;i++){
            Beer beer = beers.get(i);
            beerOrder.addBeerOrderItem(new BeerOrderItem(beer,order[i][1])); //add item
            beer.setStock(beer.getStock()-order[i][1]);
            beerRepository.updateBeer(beer);
        }
        return beerOrderRepository.saveOrder(beerOrder);
    }
}
