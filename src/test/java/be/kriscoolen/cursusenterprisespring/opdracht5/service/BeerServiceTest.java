package be.kriscoolen.cursusenterprisespring.opdracht5.service;

import be.kriscoolen.cursusenterprisespring.opdracht5.exceptions.*;
import be.kriscoolen.cursusenterprisespring.opdracht5.repository.BeerOrderRepository;
import be.kriscoolen.cursusenterprisespring.opdracht5.repository.BeerRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
public class BeerServiceTest {

    @Autowired
    BeerService beerService;

    @Autowired
    BeerRepository beerRepository;

    @Autowired
    BeerOrderRepository beerOrderRepository;

    @Test
    @Rollback
    public void orderBeerTest(){

        //test dat je een InvalidBeerException krijgt als je een bier probeert te bestellen met een id die niet in DB zit.
        assertThrows(InvalidBeerException.class,()->beerService.orderBeer("someOrder",beerRepository.findAll().size()+1,10));
        //test dat je een InvalidNumberException krijgt als je een negatief aantal bieren wil bestellen
        assertThrows(InvalidNumberException.class,()->beerService.orderBeer("myOrderName",4,-1));
        //test dat je een InvalidNumberException krijgt als je meer van een bier bestelt dan er in voorraad zit
        assertThrows(InvalidNumberException.class,()->beerService.orderBeer("orderName",4,11));

        //nu maken we een geldige bestelling.
        //we controleren eerst of de stock correct wordt ge-update.
        int orderId=0;
        try {
            orderId = beerService.orderBeer("myOrderName", 4, 7);
        }catch(Exception e){}
        assertEquals(3,beerRepository.getBeerById(4).getStock());
        //controleer ook of de BeerOrder correct wordt gesaved
        assertEquals(orderId,beerOrderRepository.getBeerOrderById(orderId).getId());
    }

    @Test
    @Rollback
    public void orderBeersTest(){
        //create 3 orders in 2-d array and make sure only last one is invalid
        //first make the last one a negative number
        int[][] order = new int[3][2];
        order[0][0]=1; order[0][1]=10; //order beer with id=1, 10 times
        order[1][0]=2; order[1][1]=20; //order beer with id=2, 20 times
        order[2][0]=3; order[2][1]=-1; //order beer with id=3, -1 times
        //before we make the order we store the current stocks to see that they are still the same after the invalid order
        ArrayList<Integer> stockList = new ArrayList<>();
        for(int i=0; i<order.length;i++){
            stockList.add(beerRepository.getBeerById(i+1).getStock());
        }
        assertThrows(InvalidNumberException.class,()->beerService.orderBeers("someName",order));
        //check if beers still have their original stock
        for(int i=0; i<order.length;i++){
            assertEquals((int)stockList.get(i),beerRepository.getBeerById(i+1).getStock());
        }
        //now we choose more than the stock of beer with id=3 (stock=50)
        order[2][1]=51;
        assertThrows(InvalidNumberException.class,()->beerService.orderBeers("someName",order));
        //check if beers still have their original stock
        for(int i=0; i<order.length;i++){
            assertEquals((int)stockList.get(i),beerRepository.getBeerById(i+1).getStock());
        }
        //finally we choose an invalid beer id for the 3-th beer (but a valid number)
        order[2][0]=10; order[2][1]=20;
        assertThrows(InvalidBeerException.class,()->beerService.orderBeers("someName",order));
        //check if beers still have their original stock
        for(int i=0; i<order.length;i++){
            assertEquals((int)stockList.get(i),beerRepository.getBeerById(i+1).getStock());
        }
        //now we make a valid order by setting the id back to id=3
        order[2][0] = 3;
        int orderId=beerService.orderBeers("someName",order);

        //see if stock is updated correctly:
        for(int i=0; i<order.length;i++){
            assertEquals(stockList.get(i)-order[i][1],beerRepository.getBeerById(i+1).getStock());
        }
        //see if order is saved correctly:
        assertEquals(orderId,beerOrderRepository.getBeerOrderById(orderId).getId());
    }
}
