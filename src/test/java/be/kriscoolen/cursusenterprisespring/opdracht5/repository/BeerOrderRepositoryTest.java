package be.kriscoolen.cursusenterprisespring.opdracht5.repository;

import be.kriscoolen.cursusenterprisespring.opdracht5.domain.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
public class BeerOrderRepositoryTest {

    @Autowired
    BeerOrderRepository beerOrderRepository;
    @Autowired
    BeerRepository beerRepository;

    @Test
    public void getBeerOrderByIdTest(){
        BeerOrder beerOrder = beerOrderRepository.getBeerOrderById(1);
        assertEquals(1,beerOrder.getId());
        assertEquals("Bestelling kris",beerOrder.getName());
        assertEquals(2,beerOrder.getItems().size());
    }

    @Test
    public void saveOrderTest(){
        //create a new beerorder with two beerorderitems
        BeerOrder beerOrder = new BeerOrder();
        beerOrder.setName("mijnBestelling");
        BeerOrderItem item1 = new BeerOrderItem();
        item1.setNumber(24);
        item1.setBeer(beerRepository.getBeerById(1));
        BeerOrderItem item2 = new BeerOrderItem();
        item2.setNumber(6);
        item2.setBeer(beerRepository.getBeerById(3));
        List<BeerOrderItem> itemList= new ArrayList<>();
        itemList.add(item1);
        itemList.add(item2);
        beerOrder.setItems(itemList);
        //save this order. Since there is only one order already in the test database, this order will have id 2
        assertEquals(2,beerOrderRepository.saveOrder(beerOrder));
        //check if beerorder has correct name and has indeed two items
        assertEquals("mijnBestelling",beerOrder.getName());
        assertEquals(2,beerOrder.getItems().size());
    }
}
