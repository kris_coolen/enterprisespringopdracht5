package be.kriscoolen.cursusenterprisespring.opdracht5.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
public class BrewerRepositoryTest {

    @Autowired
    BrewerRepository brewerRepository;

    @Test
    public void getBrewerByIdTest(){
        assertNotNull(brewerRepository.findBrewerById(1));
        assertNull(brewerRepository.findBrewerById(0));
    }
}
