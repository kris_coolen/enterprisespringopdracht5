package be.kriscoolen.cursusenterprisespring.opdracht5.repository;

import be.kriscoolen.cursusenterprisespring.opdracht5.domain.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Commit;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

//@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class BeerRepositoryTest {

    @Autowired
    private BeerRepository beerRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private BrewerRepository brewerRepository;

    @Test
    public void getBeerByIdTest(){
        Beer beer = beerRepository.getBeerById(1);
        assertEquals(1,beer.getId());
        assertEquals("chimay Triple",beer.getName());
        assertEquals(2,beer.getBrewer().getId());
        assertEquals(1,beer.getCategory().getId());
        assertEquals(2.75f,beer.getPrice());
        assertEquals(100,beer.getStock());
        assertEquals(7.0,beer.getAlcohol());
        assertEquals(0,beer.getVersion());
    }

    @Test
    public void getBeerByAlcoholTest(){
        assertEquals(2,beerRepository.getBeersByAlcohol(6.5f).size());
        assertEquals(0,beerRepository.getBeersByAlcohol(5.0f).size());
    }

    @Test
    public void updateBeerTest(){
        Beer beer = beerRepository.getBeerById(1);
        beer.setPrice(4.5f);
        beerRepository.updateBeer(beer);
        assertEquals(4.5f,beerRepository.getBeerById(1).getPrice());
        //nu een een nieuw beer object maken. Wel zeker id geven die bestaat!
        Beer beer2 = new Beer();
        beer2.setId(2);
        //hiemee willen we dus het bestaande bier in de DB met id 2 gaan updaten.
        //we passen bijvoorbeeld de prijs en stock aan.
        beer2.setPrice(5.45f);
        beer2.setStock(1000);
        beerRepository.updateBeer(beer2);
        //de overige velden zijn nu normaal niet ingevuld! dit is dus geen goede manier om een update te doen!!!!
        //maar het is wel mogelijk...
        Beer beerDB2 = beerRepository.getBeerById(2);
        assertEquals(5.45f,beerDB2.getPrice());
        assertEquals(1000,beerDB2.getStock());
        assertEquals(null,beerDB2.getName());
    }

    @Test
    public void findBeersByNameIgnoreCaseContainingOrderByNameAscTest(){
        List<Beer> chimayBeers =
                beerRepository.findBeersByNameIgnoreCaseContainingOrderByNameAsc("chim");
        assertEquals(4,chimayBeers.size());
        assertEquals("Blue Chimay Grand Cru",chimayBeers.get(0).getName());
        assertEquals(2,
                beerRepository.findBeersByNameIgnoreCaseContainingOrderByNameAsc("trip").size());

    }

    @Test
    public void findBeerByStockLessThanTest(){
        assertEquals(2,beerRepository.findBeersByStockLessThan(100).size());
        assertEquals(4,beerRepository.findBeersByStockLessThan(101).size());
    }


    @Test
    public void updatePriceTest(){
        List<Float> oldPrices = new ArrayList<>();
        List<Beer> beerList = beerRepository.findAll();
        for(Beer b: beerList) oldPrices.add(b.getPrice());
        //System.out.println("oldprices:");
       // System.out.println(oldPrices);
        assertEquals(beerList.size(),beerRepository.updatePrice(1.05f));
        List<Beer> newBeerList = beerRepository.findAll();
        List<Float> newPrices = new ArrayList<>();
        for(Beer b: newBeerList) newPrices.add(b.getPrice());
       // System.out.println("newprices:");
        //System.out.println(newPrices);
        for(int i = 0; i<oldPrices.size();i++){
            assertTrue(Math.abs(1.05f*oldPrices.get(i)-newPrices.get(i))<0.001f);
        }
    }
    @Test
    public void findBeersWithAlcoholAndStartingWithTest(){
        assertEquals(2,beerRepository.findBeersWithAlcoholAndStartingWith(9.0f,"st").size());
    }

    @Test
    public void findBeersByCategoryTest(){

        Category category = categoryRepository.findCategoryById(2);
        assertEquals(2,beerRepository.findBeersByCategory(category).size());

    }

    @Test
    public void findBeersByBrewerTest(){
        Brewer brewer = brewerRepository.findBrewerById(2);
        assertEquals(4,beerRepository.findBeersByBrewer(brewer).size());
    }
}
