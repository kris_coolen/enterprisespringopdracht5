package be.kriscoolen.cursusenterprisespring.opdracht5.repository;
import be.kriscoolen.cursusenterprisespring.opdracht5.domain.Beer;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
public class CategoryRepositoryTest {

    @Autowired
    CategoryRepository categoryRepository;


    @Test
    public void findCategoryByIdTest(){
        assertNotNull(categoryRepository.findCategoryById(1));
        assertNull(categoryRepository.findCategoryById(0));
    }

}
